FROM phusion/passenger-ruby19
MAINTAINER Jake He "zhex900@gmail.com"

ENV HOME /root
ENV RAILS_ENV production
ENV VIRTUAL_HOST wifi.churchinperth.org

#add source url 
RUN echo "deb http://us.archive.ubuntu.com/ubuntu trusty main multiverse" >> /etc/apt/sources.list

#install dependencies
RUN apt-get update && \
	apt-get -yq install mysql-client lame festival festvox-italp16k festvox-rablpc16k librsvg2-bin

RUN rm -f /etc/service/nginx/down
RUN rm /etc/nginx/sites-enabled/default
ADD nginx.conf /etc/nginx/sites-enabled/owums.conf

RUN git clone https://zhex900@bitbucket.org/zhex900/owums.git /home/app/owums

WORKDIR /home/app/owums
RUN chown -R app:app /home/app/owums
RUN sudo -u app bundle install --deployment

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

CMD ["/sbin/my_init"]
